<div align="left">

# Hey, me chamo Luann! 🏳‍🌈 <br>

- 🤸🏼 Tenho 23 anos, amo tecnologia, gatos e Lana del Rey
- 🎓 Graduando em Engenharia de Software - Jala University
<br> 
</div>
<div align="center">
  <img height="150em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=heyluannlucas&layout=compact&langs_count=7&theme=light"/>
</div>

<div align="center"> 
  <a href="mailto:Luann.deSousa@jala.university" target="_blank"><img src="https://img.shields.io/badge/-Email-%234285F4?style=for-the-badge&logo=mail.ru&logoColor=white" target="_blank"></a>
</div>
